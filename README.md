# lp graduacao hibrida


## Desenvolvimento

### Requisitos
`yarn` ou `npm`

[Live Server - VS Code](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
*Para preview do Projeto*

### Instalando

`yarn install` ou `npm install`

### Scripts

1 - Build do /scss sempre ao modificá-los

`yarn sass` ou `npm sass`

2 - Inicie o Live Preview Server

Com o arquivo index.html aberto, clique no Go Live para inicializar o Live Server

OBS: Qualquer alteração nos arquivos SCSS executará o build para /css.
